import React from "react";
import moment from "moment";
import List from "@material-ui/core/List";
import {
  ListItemText,
  IconButton,
  ListItemSecondaryAction,
  ListItem,
  Typography
} from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import { Link } from "react-router-dom";

const NotesList = ({ notes, deleteNote }) => {
  return notes.length ? (
    <List>
      {notes.map(note => (
        <ListItem key={note.id} button component={Link} to={`/view/${note.id}`}>
          <ListItemText
            primary={note.title}
            secondary={moment(note.id).format("MM Do YY")}
          />
            <ListItemSecondaryAction>
              <IconButton onClick={() => deleteNote(note.id)} >
                <DeleteIcon />
              </IconButton>
            </ListItemSecondaryAction>
        </ListItem>
      ))}
    </List>
  ) : (
    <Typography align="center" variant="h4">
      No notes yet ...
    </Typography>
  );
};

export default NotesList;
