import React, { Component, Fragment } from "react";
import { Typography } from "@material-ui/core";

const Note = ({note}) => {

    // let valor = notas.filter(({id}) => id)
    // const note = props.notes.includes(props.match.params.id)
   //  console.log(note);


  return (
    <Fragment>
      <Typography align="center" variant="h4" gutterBottom>
          {note.title}
      </Typography>
      <Typography align="justify" variant="subtitle1">
        {note.description}
      </Typography>
    </Fragment>
  );
};

export default Note;
